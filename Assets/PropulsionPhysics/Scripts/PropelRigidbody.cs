﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;

namespace Polycrime
{
    public class PropelRigidbody : MonoBehaviour, IPropelBehavior
    {
        private Text timertext;
        private float startTime;

        private Rigidbody cachedRigidbody3D;
       // private Rigidbody2D cachedRigidbody2D;
        private GameObject shapeSphere, shapeCapsule, Traingle;
        private Vector3 ShapePos;
        private System.Random getRandom = new System.Random();
        private int randomnumber;
        private List<GameObject> lstGameObjects = new List<GameObject>();
        private List<Texture> creatureTextures = new List<Texture>();
        private List<Sprite> spriteImages = new List<Sprite>();
        private List<Mesh> lstMesh = new List<Mesh>();
        private int creatureHitCnt;
        private int creatureEscapeCnt;
        private Canvas playAgainCanvas;
        void OnCollisionEnter(Collision col)
        {
            if (col.gameObject.name == "Creature")
            {                
                creatureHitCnt++;              
                col.gameObject.GetComponent<Renderer>().material.mainTexture = creatureTextures[creatureHitCnt];
                GameObject.FindGameObjectWithTag("img_creatureProgress").GetComponent<Image>().sprite = spriteImages[creatureHitCnt];
                if (creatureHitCnt == 3)
                {
                    creatureHitCnt = 0;
                    playAgainCanvas = GameObject.FindGameObjectWithTag("canvas_playAgain").GetComponent<Canvas>();
                    GameObject.FindGameObjectWithTag("img_creatureProgress").GetComponent<Image>().enabled = false;
                    playAgainCanvas.enabled = true;
                    Time.timeScale = 0;
                   
                }
            }

            if (col.gameObject.name == "Mon_Su_Edit_02:Building_7")
            {
                creatureEscapeCnt++;
                if (creatureEscapeCnt == 3)
                {
                    creatureEscapeCnt = 0;
                    Time.timeScale = 0;
                    GameObject.FindGameObjectWithTag("gesture_canvas").GetComponent<Canvas>().enabled = true;
                }

            }
            if (col.gameObject.name == "Mon_Su_Edit_02:Building_7" || col.gameObject.name == "Creature")
            {
              
                gameObject.GetComponent<Renderer>().enabled = false;
                //Destroy(gameObject)
                randomnumber = getRandom.Next(0, 3);
               
                gameObject.GetComponent<MeshFilter>().mesh = lstMesh[randomnumber];

                if (randomnumber == 0)
                    gameObject.tag = "Sphere";
                else if(randomnumber == 1)
                    gameObject.tag = "Box";
                else if(randomnumber == 2) {
                    gameObject.tag = "Traingle";
                }
                lstGameObjects[0].transform.position = new Vector3(-0.415f, 2.27f, -4.121f);
                lstGameObjects[0].GetComponent<Renderer>().enabled = true;

                cachedRigidbody3D = lstGameObjects[0].GetComponent<Rigidbody>();                
                //StartCoroutine(WaitASec(1.0F));
                //cachedRigidbody3D.velocity = ShapePos;            

            }
        }       
        public void React(Vector3 velocity)
        {

            ShapePos = velocity;

            if (cachedRigidbody3D)
            {
                StartCoroutine(WaitASec(1.0F));
                cachedRigidbody3D.velocity = ShapePos;

            }


        }

        private void Awake()
        {
            creatureHitCnt = 0;
            creatureTextures.Add((Texture)Resources.Load("diffuse"));
            creatureTextures.Add((Texture)Resources.Load("diffuse_01"));
            creatureTextures.Add((Texture)Resources.Load("diffuse_02"));
            creatureTextures.Add((Texture)Resources.Load("diffuse_03"));

            spriteImages.Add((Sprite)Resources.Load("green_bar", typeof(Sprite)) as Sprite);
            spriteImages.Add((Sprite)Resources.Load("orange_bar", typeof(Sprite)) as Sprite);
            spriteImages.Add((Sprite)Resources.Load("red_bar", typeof(Sprite)) as Sprite);
            spriteImages.Add(new Sprite());
            shapeSphere = GameObject.FindGameObjectWithTag("Sphere");
            shapeCapsule = GameObject.FindGameObjectWithTag("Box");
            Traingle = GameObject.FindGameObjectWithTag("Traingle"); 

            GameObject.FindGameObjectWithTag("propulsanpad").GetComponent<Renderer>().enabled = false;
            GameObject.FindGameObjectWithTag("cube").GetComponent<Renderer>().enabled = false;            
            shapeSphere.GetComponent<Renderer>().enabled = false;
            shapeCapsule.GetComponent<Renderer>().enabled = false;           
            Traingle.GetComponent<Renderer>().enabled = false;

            MeshFilter viewedModelFilter1 = (MeshFilter)GameObject.FindGameObjectWithTag("Sphere").GetComponent("MeshFilter");
            lstMesh.Add(viewedModelFilter1.mesh);
            MeshFilter viewedModelFilter2 = (MeshFilter)GameObject.FindGameObjectWithTag("Box").GetComponent("MeshFilter");
            lstMesh.Add(viewedModelFilter2.mesh);
            MeshFilter viewedModelFilter3 = (MeshFilter)GameObject.FindGameObjectWithTag("Traingle").GetComponent("MeshFilter");
            lstMesh.Add(viewedModelFilter3.mesh);


            lstGameObjects.Add(shapeSphere);
            lstGameObjects.Add(shapeCapsule);
            lstGameObjects.Add(Traingle);


            randomnumber = getRandom.Next(0, 3);
            lstGameObjects[0].transform.position = new Vector3(-0.415f, 2.27f, -4.121f);
            lstGameObjects[0].GetComponent<Renderer>().enabled = true;
            cachedRigidbody3D = lstGameObjects[0].GetComponent<Rigidbody>();

            //cachedRigidbody3D = shapeSphere.GetComponent<Rigidbody>();
        }

        IEnumerator WaitASec(float waitTime)
        {
            yield return new WaitForSeconds(waitTime);
            Debug.Log("Waited a sec");
           
        }

        void Start()
    {
            timertext = GameObject.FindGameObjectWithTag("timer_text").GetComponent<Text>();
            startTime = Time.time;
    }

        void Update()
        {
            float t = Time.time - startTime;
            string minutes = ((int)t / 60).ToString();
            string seconds = (t % 60).ToString("f0");
            //timertext.text = "0" + minutes + ":" + seconds; //(seconds < 10 ? "0" + seconds : seconds);
            
            if (Time.timeScale == 0)
            {
                startTime = Time.time;
            }
            else
            {
                timertext.text = "0" + minutes + ":" + (Convert.ToInt32(seconds) < 10 ? "0" + seconds : seconds);
            }

        }

    }
  
}

