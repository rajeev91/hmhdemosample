﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayAgain : MonoBehaviour {
    public Canvas playAgainCanvas;
    public bool togle = false;



    public void drawpadPopup()
    {
        playAgainCanvas = GameObject.FindGameObjectWithTag("canvas_playAgain").GetComponent<Canvas>();
        playAgainCanvas.enabled = false;
        Time.timeScale = 1;
        GameObject.FindGameObjectWithTag("img_creatureProgress").GetComponent<Image>().enabled = true;
        GameObject.FindGameObjectWithTag("creature").GetComponent <Renderer>().material.mainTexture = (Texture)Resources.Load("diffuse");
        GameObject.FindGameObjectWithTag("img_creatureProgress").GetComponent<Image>().sprite = (Sprite)Resources.Load("green_bar", typeof(Sprite)) as Sprite;
   

    }
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
